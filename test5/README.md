# 实验5：包，过程，函数的用法
姓名：李世博  软件工程4班  学号：202010414408

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 脚本代码参考

```sql
create or replace PACKAGE MyPack IS

FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); 
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```
![](pict1.png)

*上段代码是创建一个名为 "MyPack" 的包，其中包含一个名为 "Get_SalaryAmount" 的函数和一个名为 "Get_Employees" 的过程。  *  

*"Get_SalaryAmount" 函数接受一个部门 ID，返回该部门的所有员工薪资总和。    *  

*"Get_Employees" 过程接受一个员工 ID，递归查询该员工及其下属，并以树形结构输出。*

## 测试

```sql
函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

输出：
DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
           10 Administration                 4848
           20 Marketing                      19896
           30 Purchasing                     27588
           40 Human Resources                6948
           50 Shipping                       176560

```
![](pict2.png)

这是一条SELECT语句，用于查询departments表中每个部门的部门编号（department_id）、部门名称（department_name）和该部门的员工薪资总额（salary_total），其中薪资总额的计算是通过调用名为MyPack.Get_SalaryAmount的函数实现的。

假设Get_SalaryAmount函数是一个自定义的PL/SQL函数，它的作用是计算指定部门的员工薪资总额。该函数需要接收一个参数，即部门编号，返回该部门的员工薪资总额。通过调用该函数，并将返回值作为查询结果中的一个列，可以方便地获取每个部门的员工薪资总额。

```sql
过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/

输出：
101 Neena
    108 Nancy
        109 Daniel
        110 John
        111 Ismael
        112 Jose Manuel
        113 Luis
    200 Jennifer
    203 Susan
    204 Hermann
    205 Shelley
        206 William
```
![](pict3.png)
这是一个PL/SQL的代码块，用于调用名为MYPACK.Get_Employees的存储过程，并传递一个参数V_EMPLOYEE_ID。该代码块中的SET SERVEROUTPUT ON语句用于打开服务器输出，以便在执行代码块时输出调试信息或结果。

存储过程MYPACK.Get_Employees的作用是查询指定员工的详细信息，并输出到服务器输出。该存储过程的参数为一个员工编号V_EMPLOYEE_ID，通过该参数可以指定需要查询的员工。

在该代码块中，首先声明了一个变量V_EMPLOYEE_ID并初始化为101，然后调用MYPACK.Get_Employees存储过程，并将V_EMPLOYEE_ID作为参数传递给该存储过程。最后，代码块执行完毕后，可以在服务器输出中查看MYPACK.Get_Employees存储过程的输出结果。

请注意，该代码块并没有输出任何结果到客户端，只是将MYPACK.Get_Employees存储过程的输出结果输出到了服务器输出中，需要使用相应的工具或语句查看服务器输出。

- 使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工:

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```
![](pict.png)

这是一条SQL查询语句，用于查询员工表（employees）中给定员工（V_EMPLOYEE_ID）及其所有上级经理的信息。查询结果包括员工的级别（LEVEL）、员工编号（EMPLOYEE_ID）、员工名字（FIRST_NAME）以及该员工的上级经理编号（MANAGER_ID）。

该查询语句使用了递归查询（CONNECT BY），通过使用START WITH关键字和PRIOR运算符来建立员工之间的上下级关系。具体来说，START WITH EMPLOYEE_ID = V_EMPLOYEE_ID指定了起始的员工编号，CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID指定了如何建立员工之间的关系，即通过比较前一个员工的员工编号（PRIOR EMPLOYEE_ID）是否等于当前员工的上级经理编号（MANAGER_ID）来建立关系。该查询语句使用LEVEL伪列来获取每个员工的级别，表示该员工距离起始员工（V_EMPLOYEE_ID）的层级深度。

总之，该查询语句可以返回指定员工及其所有上级经理的信息，以便进行管理和分析。

## 实验总结
1. 本次实验的主要目的是让学生能够熟练掌握PL/SQL语言结构、变量和常量的声明和使用方法，以及包、过程和函数的用法。    

下面是PL/SQL语言结构：
PL/SQL是一种过程化编程语言，主要用于Oracle数据库系统。它是一种结构化语言，其语法和语言结构包括以下几种：

（1）匿名块（Anonymous block）：PL/SQL中最简单的程序单元，是一个可以独立执行的代码块。它由DECLARE、BEGIN和END三个部分组成，可以用来定义变量、常量、游标等，并执行一些操作。

（2）存储过程（Stored procedure）：存储过程是一组预编译的SQL语句和程序逻辑，可以在需要时通过调用来执行。它通常被用于实现业务逻辑、数据校验等功能。

（3）存储函数（Stored function）：存储函数与存储过程类似，但它返回一个值，可以作为表达式的一部分使用。存储函数也可以用于实现业务逻辑、数据校验等功能。

（4）触发器（Trigger）：触发器是一段自动执行的PL/SQL代码，可以在表中插入、更新或删除数据时自动触发。它可以用于实现复杂的数据逻辑、数据校验等功能。

（5）游标（Cursor）：游标是一种数据访问机制，允许程序员在对数据库进行操作时逐行访问结果集。它可以用于处理大量数据、数据过滤和排序等功能。

（6）异常处理（Exception handling）：异常处理是PL/SQL中的一项重要功能，用于处理程序在运行过程中出现的异常情况。可以通过定义异常和使用异常处理程序来捕获并处理异常。

（7）包（Package）：包是一种将相关程序逻辑组织在一起的方式，可以包含存储过程、存储函数、游标、变量、常量等，用于提高程序的可维护性和可重用性。

（8）语句（Statement）：PL/SQL中包含多种语句，如控制语句、循环语句、条件语句、赋值语句等，用于实现具体的程序逻辑。

这些语言结构是PL/SQL的基础，开发者可以根据实际需求选择合适的语言结构来实现程序功能。   
在PL/SQL中，变量和常量可以用来存储和操作数据。变量的值可以在程序执行中被改变，而常量的值是固定不变的。下面介绍PL/SQL中变量和常量的声明和使用方法。

2. 变量的声明和使用

在PL/SQL中，变量可以通过DECLARE语句进行声明，例如：

```sql
DECLARE
    var_name datatype;
```

其中，var_name为变量名称，datatype为变量的数据类型。变量的数据类型可以是数值型、字符型、日期型等。例如，声明一个名为age的整型变量：

```sql
DECLARE
    age INTEGER;
```

变量的值可以通过赋值语句进行修改，例如：

```sql
age := 18;
```

变量也可以在语句中使用，例如：

```sql
IF age > 18 THEN
   -- do something
END IF;
```

2. 常量的声明和使用

在PL/SQL中，常量可以通过CONSTANT关键字进行声明，例如：

```sql
DECLARE
    constant_name CONSTANT datatype := value;
```

其中，constant_name为常量名称，datatype为常量的数据类型，value为常量的值。例如，声明一个名为PI的常量：

```sql
DECLARE
    PI CONSTANT NUMBER(3, 2) := 3.14;
```

常量的值不可被修改，只能被读取。常量的值可以在语句中使用，例如：

```sql
DECLARE
    PI CONSTANT NUMBER(3, 2) := 3.14;
    radius NUMBER(3, 2) := 2.5;
BEGIN
    dbms_output.put_line('圆的周长为：' || 2 * PI * radius);
END;
```
 
通过变量和常量的声明和使用，PL/SQL可以实现数据的存储和操作，可以方便地实现程序逻辑。    
在Oracle数据库中，包、过程和函数是三种常用的PL/SQL编程对象，它们可以被用来封装复杂的逻辑，提高程序的可重用性和可维护性。

3. 包（Package）

包是一种存储过程、函数、变量和游标等程序对象的容器，可以将多个相关的程序对象组合在一起，实现更好的程序封装和管理。一个包包含两个部分：规范（specification）和体（body）。

规范定义了包中所包含的程序对象的接口，包括过程、函数和变量的声明等，规范部分通常不包含程序逻辑。体定义了实现规范部分中的程序对象的实际代码。包可以在数据库中保存为一个单独的对象，可以被其他程序调用和使用。

4. 过程（Procedure）

过程是一种PL/SQL程序对象，用于执行一系列的操作，通常不返回任何值。过程可以包含控制语句、SQL语句、条件语句等，可以通过参数传递数据。过程可以被其他PL/SQL程序和SQL语句调用。

以下是一个简单的过程示例，用于向employee表中插入一条记录：

```sql
CREATE OR REPLACE PROCEDURE insert_employee ( 
    emp_name IN VARCHAR2, 
    emp_salary IN NUMBER ) 
AS 
BEGIN 
    INSERT INTO employee (name, salary) 
    VALUES (emp_name, emp_salary); 
    COMMIT; 
END insert_employee;
```

该过程的参数包括员工姓名和薪资，通过INSERT语句向employee表中插入一条记录，最后提交事务。

5. 函数（Function）

函数是一种PL/SQL程序对象，用于执行一系列的操作，并返回一个值。函数可以包含控制语句、SQL语句、条件语句等，可以通过参数传递数据。函数可以被其他PL/SQL程序和SQL语句调用。

以下是一个简单的函数示例，用于查询employee表中薪资最高的员工姓名：

```sql
CREATE OR REPLACE FUNCTION get_highest_salary RETURN VARCHAR2 
AS 
    emp_name VARCHAR2(100); 
    emp_salary NUMBER(8,2); 
BEGIN 
    SELECT name, salary INTO emp_name, emp_salary 
    FROM employee 
    WHERE salary = (SELECT MAX(salary) FROM employee); 
    RETURN emp_name; 
END get_highest_salary;
```

该函数使用SELECT语句查询employee表中薪资最高的员工姓名，最后返回员工姓名。

通过包、过程和函数的使用，可以将复杂的程序逻辑封装成易于管理和调用的对象，提高程序的可重用性和可维护性。同时，PL/SQL中还提供了许多其他的编程对象，如触发器、游标等，可以根据实际需要选择合适的编程对象来实现程序逻辑。

