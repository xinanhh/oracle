# 实验2：用户及权限管理

- 学号：202010414408   姓名：李世博  班级：2020级软件工程4班

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色cnn_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有cnn_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sales_force，给用户分配表空间，设置限额为50M，授予cnn_res_role角色。
- 最后测试：用新用户sales_force连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。
  - 第1步：以system登录到pdborcl，创建角色cnn_res_role和用户sales_force，并授权和分配空间：


```sql
$ sqlplus system/123@pdborcl
CREATE ROLE cnn_res_role;
GRANT connect,resource,CREATE VIEW TO cnn_res_role;
CREATE USER sales_force IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER sales_force default TABLESPACE "USERS";
ALTER USER sales_force QUOTA 50M ON users;
GRANT cnn_res_role TO sales_force;
# 给用户分配创建session的权限
grant session to sales_force;
--收回角色
REVOKE cnn_res_role FROM sales_force;
```
![pict1](https://gitlab.com/xinanhh/oracle/-/raw/main/test2/pict1.png?inline=false)
> 语句“ALTER USER sales_force QUOTA 50M ON users;”是指授权sales_force用户访问users表空间，空间限额是50M。

- 第2步：新用户sales_force连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```sql
$ sqlplus sales_force/123@pdborcl
SQL> show user;
USER is "sales_force"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'Alice');
INSERT INTO customers(id,name)VALUES (2,'Bob');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
```

![pict2](https://gitlab.com/xinanhh/oracle/-/raw/main/test2/pict1.png?inline=false)

- 第3步：用户hr连接到pdborcl，查询sales_force授予它的视图customers_view

```sql
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM sales_force.customers;
elect * from sales_force.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在

SQL> SELECT * FROM sales_force.customers_view;
NAME
--------------------------------------------------
Alice
Bob
```

> 测试一下用户hr,sales_force之间的表的共享，只读共享和读写共享都测试一下。
> sales_force用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。
>

![pict3](https://gitlab.com/xinanhh/oracle/-/raw/main/test2/pict3.png?inline=false)



## 概要文件设置,用户最多登录时最多只能错误3次

```sql
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```

- ![pict4](https://gitlab.com/xinanhh/oracle/-/raw/main/test2/pict4.png?inline=false)
- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- ![pict5](https://gitlab.com/xinanhh/oracle/-/raw/main/test2/pict5.png?inline=false)
- 锁定后，通过system用户登录，alter user hr unlock命令解锁。

```sh
$ sqlplus system/123@pdborcl
SQL> alter user sales_force  account unlock;

alter user hr  account unlock;

```

![pict6](https://gitlab.com/xinanhh/oracle/-/raw/main/test2/pict6.png?inline=false)

解除锁定之后，可以正常登录。

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色cnn_res_role和用户sales_force。
> 新用户sales_force使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。



![pict7](https://gitlab.com/xinanhh/oracle/-/raw/main/test2/pict7.png?inline=false)

```sql
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';


SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
```

- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。

![pict8](https://gitlab.com/xinanhh/oracle/-/raw/main/test2/pict8.png?inline=false)

## 实验结束删除用户和角色

```sh
$ sqlplus system/123@pdborcl
SQL>
drop role cnn_res_role;
drop user sales_force cascade;
```

![pict9](https://gitlab.com/xinanhh/oracle/-/raw/main/test2/pict9.png?inline=false)

## 结论

本次Oracle实验主要围绕用户管理、角色管理、权限维护与分配、用户之间共享对象以及概要文件对用户的限制展开,先分别介绍以上概念：  

1.用户管理：在Oracle中，用户管理包括创建、修改、禁用和删除用户。每个用户都有自己的用户名和密码，以及相应的权限。管理员可以创建不同类型的用户，例如管理员用户、普通用户等，以便为不同类型的用户提供不同的权限。  

2.角色管理：角色是一组权限的集合，可以授权给一个或多个用户。管理员可以创建角色并授予它们不同的权限。然后，管理员可以将角色授予一个或多个用户，以便为这些用户提供相应的权限。  

3.权限维护与分配：Oracle中的权限是以对象为基础的，例如表、视图、存储过程等。管理员可以为每个对象授予不同的权限，例如SELECT、INSERT、UPDATE和DELETE等。管理员还可以将这些权限授予用户或角色，以便为它们提供相应的访问权限。  

4.用户之间共享对象：在Oracle中，一个用户可以将一个对象授权给另一个用户。这意味着，一个用户可以让另一个用户访问他拥有的对象，而不需要再次创建相同的对象。这种共享对象的方法称为“授权”。  

5.概要文件对用户的限制：在Oracle中，可以使用概要文件来限制用户的资源使用。概要文件定义了许多限制，例如最大连接数、最大CPU时间、最大空间使用量等。管理员可以将概要文件分配给不同的用户，以便为他们设置资源使用的限制。这样可以确保用户不会消耗过多的系统资源，从而影响其他用户的性能。  

通过本次实验，我知道了用户管理、角色管理、权限维护与分配、用户之间共享对象以及概要文件对用户的限制是Oracle数据库管理中非常重要的方面，通过正确配置和管理这些功能，管理员可以确保数据库的安全和性能，并确保用户只能访问他们需要的数据和资源，这次实际操作让我对Oracle数据库有了新的认识。
