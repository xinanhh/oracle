# 实验4：PL/SQL语言打印杨辉三角

- 学号：202010414408 姓名：李世博 班级：2020级软工4班

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。

```sql
create or replace procedure YHTriange(N in integer) 
is
   type t_number is varray (100) of integer not null; --数组
    i integer;
    j integer;
    spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
   -- N integer := 9; -- 一共打印9行数字
    rowArray t_number := t_number();
begin
   dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
end YHTriange;
```

调用存储过程：

```sql

set serveroutput on;
declare
   begin
      YHTriange(10); 
   end;
```

截图：
![pict1](https://gitlab.com/xinanhh/oracle/-/raw/main/test4/pict1.png?inline=false)运行结果

## 实验总结

在上面的程序中，我们首先定义了一个变量 N，它表示杨辉三角的行数。接着，我们创建了一个字符串数组，用来存储每一行的数据。在循环中，我们首先将每一行的第一个元素赋值为1，然后从第二个元素开始，依次计算每个元素的值，并存储到数组中。最后，我们使用 DBMS_OUTPUT.PUT_LINE 函数将每一行的数据打印出来，很少用这种语言编写程序，所以还是有点陌生。  

PL/SQL是一种Oracle数据库专用的过程性编程语言，它结合了SQL语句和过程控制语句，可以编写存储过程、触发器、函数和包等程序对象。下面是PL/SQL语言和存储过程的一些特点：
强大的数据处理能力：PL/SQL具有强大的数据处理能力，可以方便地访问数据库中的数据，以及进行数据处理、转换和计算等操作。

高效的性能：PL/SQL可以使用数据库引擎的优化器，将SQL语句进行优化，从而提高查询和数据操作的性能。

可重用性：PL/SQL程序可以封装为存储过程、函数和包等对象，可以在不同的应用程序中重复使用，提高了程序的开发效率和可维护性。

支持事务处理：PL/SQL支持事务处理，可以使用COMMIT和ROLLBACK语句控制事务的提交和回滚，保证数据的一致性和完整性。

安全性：PL/SQL程序可以控制对数据库的访问权限，提高了数据库的安全性。

异常处理：PL/SQL可以捕获和处理程序中的异常，可以有效地防止程序因为异常而终止。

执行控制：PL/SQL提供了条件控制语句和循环语句等结构，可以对程序的执行进行精细的控制，实现更加复杂的逻辑处理。

总之，PL/SQL是一种功能强大的过程性编程语言，可以提供高效、可重用、安全的数据库操作和数据处理能力，尤其适合用于Oracle数据库应用程序的开发和维护。在课下尽量多花时间去了解和学习。
